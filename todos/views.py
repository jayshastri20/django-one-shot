from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def show_todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item": todo,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save(False)
            todo.user = request.user
            todo.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm(instance=todo)

    context = {
        "todo": todo,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        item = TodoItemForm(request.POST)
        if item.is_valid():
            new_item = item.save(False)
            new_item.save()
            return redirect("todo_list_detail", new_item.list.id)
    else:
        item = TodoItemForm()
    context = {
        "item": item,
    }
    return render(request, "items/create.html", context)


def update_todo_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save(False)
            item.user = request.user
            item.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "item": item,
        "form": form,
    }
    return render(request, "items/edit.html", context)
